package com.selfiewith.messi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;
import com.selfiewith.messi.base.BaseActivity;
import com.selfiewith.messi.filters.FilterListener;
import com.selfiewith.messi.filters.FilterViewAdapter;
import com.selfiewith.messi.tools.EditingToolsAdapter;
import com.selfiewith.messi.tools.PhotoTool;
import com.selfiewith.messi.tools.ToolType;

import java.io.File;
import java.io.IOException;

import io.github.controlwear.virtual.joystick.android.JoystickView;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.PhotoFilter;
import ja.burhanrashid52.photoeditor.ViewType;

public class EditImageActivity extends BaseActivity implements OnPhotoEditorListener,
    View.OnClickListener,
    PropertiesBSFragment.Properties,
    EmojiBSFragment.EmojiListener,
    StickerBSFragment.StickerListener, EditingToolsAdapter.OnItemSelected, FilterListener {

    private static final String TAG = EditImageActivity.class.getSimpleName();
    private PhotoEditor mPhotoEditor;
    private PhotoEditorView mPhotoEditorView;
    private PropertiesBSFragment mPropertiesBSFragment;
    private EmojiBSFragment mEmojiBSFragment;
    private StickerBSFragment mStickerBSFragment;
    private RecyclerView mRvTools, mRvFilters;
    private EditingToolsAdapter mEditingToolsAdapter = new EditingToolsAdapter(this);
    private FilterViewAdapter mFilterViewAdapter = new FilterViewAdapter(this);
    private ConstraintLayout mRootView;
    private ConstraintSet mConstraintSet = new ConstraintSet();
    private boolean mIsFilterVisible;
    private CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeFullScreen();
        setContentView(R.layout.activity_edit_image);

        initViews();

        mPropertiesBSFragment = new PropertiesBSFragment();
        mEmojiBSFragment = new EmojiBSFragment();
        mStickerBSFragment = new StickerBSFragment();
        mStickerBSFragment.setStickerListener(this);
        mEmojiBSFragment.setEmojiListener(this);
        mPropertiesBSFragment.setPropertiesChangeListener(this);

        LinearLayoutManager llmTools = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvTools.setLayoutManager(llmTools);
        mRvTools.setAdapter(mEditingToolsAdapter);

        LinearLayoutManager llmFilters = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRvFilters.setLayoutManager(llmFilters);
        mRvFilters.setAdapter(mFilterViewAdapter);


        //Typeface mTextRobotoTf = ResourcesCompat.getFont(this, R.font.roboto_medium);
        //Typeface mEmojiTypeFace = Typeface.createFromAsset(getAssets(), "emojione-android.ttf");

        mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
            .setPinchTextScalable(true) // set flag to make text scalable when pinch
            //.setDefaultTextTypeface(mTextRobotoTf)
            //.setDefaultEmojiTypeface(mEmojiTypeFace)
            .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);

        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest;
        if (BuildConfig.DEBUG) {
            adRequest = new AdRequest.Builder()
                .addTestDevice("BD1C60E379701FB989CE8D2BDBEE9501")
                .addTestDevice("66BA854E23314EEC40CC8BAE08A14716")
                .build();
        } else {
            adRequest = new AdRequest.Builder()
                .build();
        }
        mAdView.loadAd(adRequest);
    }

    private void initViews() {
        ImageView imgUndo;
        ImageView imgRedo;
        ImageView imgCamera;
        ImageView imgGallery;
        ImageView imgSave;
        ImageView imgClose;

        mPhotoEditorView = findViewById(R.id.photoEditorView);
        //mTxtCurrentTool = findViewById(R.id.txtCurrentTool);
        mRvTools = findViewById(R.id.rvConstraintTools);
        mRvFilters = findViewById(R.id.rvFilterView);
        mRootView = findViewById(R.id.rootView);

        imgUndo = findViewById(R.id.imgUndo);
        imgUndo.setOnClickListener(this);

        imgRedo = findViewById(R.id.imgRedo);
        imgRedo.setOnClickListener(this);

        imgCamera = findViewById(R.id.imgCamera);
        imgCamera.setOnClickListener(this);

        imgGallery = findViewById(R.id.imgGallery);
        imgGallery.setOnClickListener(this);

        imgSave = findViewById(R.id.imgSave);
        imgSave.setOnClickListener(this);

        imgClose = findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        Button buttonGallery = findViewById(R.id.buttonGallery);
        buttonGallery.setOnClickListener(this);
        Button buttonCamera = findViewById(R.id.buttonCamera);
        buttonCamera.setOnClickListener(this);

        JoystickView joystick = findViewById(R.id.joystickView);
        joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                int direction = angle == 0 ? 1 : -1;
                mPhotoEditorView.scrollView.lock = true;
                mPhotoEditorView.scrollView.scrollBy(direction * strength / 5, 0);
            }
        });
    }

    @Override
    public void onEditTextChangeListener(final View rootView, String text, int colorCode) {
        TextEditorDialogFragment textEditorDialogFragment =
            TextEditorDialogFragment.show(this, text, colorCode);
        textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
            @Override
            public void onDone(String inputText, int colorCode) {
                mPhotoEditor.editText(rootView, inputText, colorCode);
                //mTxtCurrentTool.setText(R.string.label_text);
            }
        });
    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        Log.d(TAG, "onAddViewListener() called with: viewType = [" + viewType + "], numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {
        Log.d(TAG, "onRemoveViewListener() called with: numberOfAddedViews = [" + numberOfAddedViews + "]");
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStartViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
        Log.d(TAG, "onStopViewChangeListener() called with: viewType = [" + viewType + "]");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgUndo:
                mPhotoEditor.undo();
                break;
            case R.id.imgRedo:
                mPhotoEditor.redo();
                break;
            case R.id.imgSave:
                saveImage();
                break;
            case R.id.imgClose:
                if (mIsFilterVisible) {
                    showFilter(false);
                } else {
                    mPhotoEditor.clearAllViews();
                    mPhotoEditorView.getSource().setImageDrawable(null);
                    findViewById(R.id.buttonGallery).setVisibility(View.VISIBLE);
                    findViewById(R.id.buttonCamera).setVisibility(View.VISIBLE);
                }
                break;
            case R.id.imgGallery: case R.id.buttonGallery:
                disposable.clear();
                disposable.add(RxImagePicker.with(getFragmentManager())
                    .requestImage(Sources.GALLERY)
                    .subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) {
                            try {
                                Bitmap photo = PhotoTool.handleSamplingAndRotationBitmap(getApplicationContext(), uri,
                                    mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight());
                                findViewById(R.id.buttonGallery).setVisibility(View.GONE);
                                findViewById(R.id.buttonCamera).setVisibility(View.GONE);
                                mPhotoEditor.clearAllViews();
                                mPhotoEditorView.scrollView.lock = false;
                                mPhotoEditorView.getSource().setImageBitmap(photo);
                                mPhotoEditorView.getSource().setScaleType(ImageView.ScaleType.CENTER_CROP);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                break;
            case R.id.imgCamera: case R.id.buttonCamera:
                disposable.clear();
                disposable.add(RxImagePicker.with(getFragmentManager())
                    .requestImage(Sources.CAMERA)
                    .subscribe(new Consumer<Uri>() {
                        @Override
                        public void accept(@NonNull Uri uri) {
                            try {
                                Bitmap photo = PhotoTool.handleSamplingAndRotationBitmap(getApplicationContext(), uri,
                                    mPhotoEditorView.getWidth(), mPhotoEditorView.getHeight());
                                findViewById(R.id.buttonGallery).setVisibility(View.GONE);
                                findViewById(R.id.buttonCamera).setVisibility(View.GONE);
                                mPhotoEditor.clearAllViews();
                                mPhotoEditorView.scrollView.lock = false;
                                mPhotoEditorView.getSource().setImageBitmap(photo);
                                mPhotoEditorView.getSource().setScaleType(ImageView.ScaleType.CENTER_CROP);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }));
                break;
        }
    }

    @SuppressLint("MissingPermission")
    private void saveImage() {
        if (requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            showLoading("Saving...");


           /* File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "SelfieWithRonaldo");

            boolean success = true;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }
            if (success) {*/

            File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "/DCIM/SelfieWithMessi");
            folder.mkdirs();

            File file = new File(Environment.getExternalStorageDirectory() + "/DCIM/SelfieWithMessi/"
                + File.separator +
                +System.currentTimeMillis() + ".png");

            MediaScannerConnection
                .scanFile(EditImageActivity.this, new String[]{file.toString()},
                    null, null);

            try {
                //folder.mkdirs();
                file.createNewFile();
                mPhotoEditor.saveAsFile(file.getAbsolutePath(), new PhotoEditor.OnSaveListener() {
                    @Override
                    public void onSuccess(@NonNull String imagePath) {
                        hideLoading();
                        showSnackbar("Image saved in DCIM folder");
                        mPhotoEditorView.getSource().setImageURI(Uri.fromFile(new File(imagePath)));
                    }

                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        hideLoading();
                        showSnackbar("Please try again");
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
                hideLoading();
                showSnackbar(e.getMessage());
            }
            // } else {
            // Do something else on failure
            // }

        }
    }

    @Override
    public void onColorChanged(int colorCode) {
        mPhotoEditor.setBrushColor(colorCode);
        //mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onOpacityChanged(int opacity) {
        mPhotoEditor.setOpacity(opacity);
        //mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onBrushSizeChanged(int brushSize) {
        mPhotoEditor.setBrushSize(brushSize);
        // mTxtCurrentTool.setText(R.string.label_brush);
    }

    @Override
    public void onEmojiClick(String emojiUnicode) {
        mPhotoEditor.addEmoji(emojiUnicode);
        // mTxtCurrentTool.setText(R.string.label_emoji);

    }

    @Override
    public void onStickerClick(Bitmap bitmap) {
        mPhotoEditor.addImage(bitmap);
        // mTxtCurrentTool.setText(R.string.label_sticker);
    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {
        // TODO logic
    }

    private void showSaveDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you want to exit without saving image ?");
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveImage();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("Discard", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create().show();

    }

    @Override
    public void onFilterSelected(PhotoFilter photoFilter) {
        mPhotoEditor.setFilterEffect(photoFilter);
    }

    @Override
    public void onToolSelected(ToolType toolType) {
        switch (toolType) {
            case BRUSH:
                mPhotoEditor.setBrushDrawingMode(true);
                //  mTxtCurrentTool.setText(R.string.label_brush);
                if (!mPropertiesBSFragment.isAdded()) {
                    mPropertiesBSFragment.show(getSupportFragmentManager(), mPropertiesBSFragment.getTag());
                }
                break;
            case TEXT:
                TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(this);
                textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                    @Override
                    public void onDone(String inputText, int colorCode) {
                        mPhotoEditor.addText(inputText, colorCode);
                        //          mTxtCurrentTool.setText(R.string.label_text);
                    }
                });
                break;
            case ERASER:
                mPhotoEditor.brushEraser();
                //  mTxtCurrentTool.setText(R.string.label_eraser);
                break;
            case FILTER:
                //  mTxtCurrentTool.setText(R.string.label_filter);
                showFilter(true);
                break;
            case EMOJI:
                if (!mEmojiBSFragment.isAdded()) {
                    mEmojiBSFragment.show(getSupportFragmentManager(), mEmojiBSFragment.getTag());
                }
                break;
            case STICKER:
                if (!mStickerBSFragment.isAdded()) {
                    mStickerBSFragment.show(getSupportFragmentManager(), mStickerBSFragment.getTag());
                }
                break;
        }
    }


    void showFilter(boolean isVisible) {
        mIsFilterVisible = isVisible;
        mConstraintSet.clone(mRootView);

        if (isVisible) {
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                ConstraintSet.PARENT_ID, ConstraintSet.START);
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.END,
                ConstraintSet.PARENT_ID, ConstraintSet.END);
        } else {
            mConstraintSet.connect(mRvFilters.getId(), ConstraintSet.START,
                ConstraintSet.PARENT_ID, ConstraintSet.END);
            mConstraintSet.clear(mRvFilters.getId(), ConstraintSet.END);
        }

        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.setDuration(350);
        changeBounds.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        TransitionManager.beginDelayedTransition(mRootView, changeBounds);

        mConstraintSet.applyTo(mRootView);
    }

    @Override
    public void onBackPressed() {
        if (mIsFilterVisible) {
            showFilter(false);
            //  mTxtCurrentTool.setText(R.string.app_name);
        } else if (!mPhotoEditor.isCacheEmpty()) {
            showSaveDialog();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
